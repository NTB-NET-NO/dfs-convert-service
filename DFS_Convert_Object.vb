Imports System.IO
Imports System.text
Imports System.text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports ntb_FuncLib


Public Class DFS_Convert_Object
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        'wordAppObject = New Application

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()

        'wordAppObject.Quit()
        'wordAppObject = Nothing
    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents FileWatcher As System.IO.FileSystemWatcher
    Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.FileWatcher = New System.IO.FileSystemWatcher
        Me.PollTimer = New System.Timers.Timer
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 5000
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Protected inputFolder As String
    Protected outputFolder As String
    Protected logFolder As String
    Protected doneFolder As String
    Protected errorFolder As String

    Protected IPTC_Sequence As Integer
    Protected IPTC_SequenceFile As String
    Protected IPTC_Range As String

    Public Sub Initiate()
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        logFolder = AppSettings("logFolder")
        If Not Directory.Exists(logFolder) Then Directory.CreateDirectory(logFolder)

        LogFile.WriteLogNoDate(logFolder, "---------------------------------------------------------------------------------")
        LogFile.WriteLog(logFolder, "DNMI Import service initiating...")

        inputFolder = AppSettings("inputFolder")
        outputFolder = AppSettings("outputFolder")
        doneFolder = AppSettings("doneFolder")
        errorFolder = AppSettings("errorFolder")

        If Not Directory.Exists(inputFolder) Then Directory.CreateDirectory(inputFolder)
        If Not Directory.Exists(outputFolder) Then Directory.CreateDirectory(outputFolder)
        If Not Directory.Exists(doneFolder) Then Directory.CreateDirectory(doneFolder)
        If Not Directory.Exists(errorFolder) Then Directory.CreateDirectory(errorFolder)

        IPTC_Range = AppSettings("IPTCRange")
        IPTC_SequenceFile = AppSettings("IPTCSequenceFile")

        IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        LoadSequence(IPTC_SequenceFile, True)

        Try
            FileWatcher.Path = inputFolder
            PollTimer.Start()
        Catch ex As Exception
            LogFile.WriteErr(logFolder, "Error starting service", ex)
        End Try

        LogFile.WriteLog(logFolder, "DNMI Import service started.")
    End Sub

    Private Sub LoadSequence(ByVal file As String, ByVal load As Boolean)
        Try
            If load Then
                IPTC_Sequence = LogFile.ReadFile(file).Trim()
                LogFile.WriteLog(logFolder, "IPTC Sequence number loaded: " & IPTC_Sequence)
            Else
                LogFile.WriteFile(file, IPTC_Sequence, False)
                LogFile.WriteLog(logFolder, "IPTC Sequence number saved: " & IPTC_Sequence)
            End If
        Catch ex As Exception
            LogFile.WriteErr(logFolder, "Error accessing IPTC sequence number.", ex)
        End Try
    End Sub

    Private Function Get_Seq() As Integer

        'Check value
        If IPTC_Sequence > CInt(IPTC_Range.Split("-")(1)) Or _
           IPTC_Sequence < CInt(IPTC_Range.Split("-")(0)) Then
            IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        End If

        'Return value
        Dim ret As Integer = IPTC_Sequence

        'Increment
        IPTC_Sequence += 1

        'Save
        LoadSequence(IPTC_SequenceFile, False)

        Return ret
    End Function

    'Stop events
    Public Sub StopEvents()
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False

        'Save the sequence-number
        LoadSequence(IPTC_SequenceFile, False)

        LogFile.WriteLog(logFolder, "DNMI Import service stopped.")
    End Sub

    'Timed polling
    Private Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False

        DoAllFiles()

        PollTimer.Interval = AppSettings("pollingInterval") * 1000
        PollTimer.Start()
        FileWatcher.EnableRaisingEvents = True
    End Sub

    'Event polling
    Private Sub FileWatcher_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileWatcher.Created
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False

        DoAllFiles()

        PollTimer.Interval = AppSettings("pollingInterval") * 1000
        PollTimer.Start()
        FileWatcher.EnableRaisingEvents = True
    End Sub

    'Loops and handles all the files in the input folder
    Protected Sub DoAllFiles()

        'Get files
        Dim files As String() = Directory.GetFiles(inputFolder)

        'Wait.... wait... wait.... 
        System.Threading.Thread.Sleep(3000)

        'Go...
        Dim f As String
        For Each f In files
            DoOneFile(f)
        Next

    End Sub

    'Handles one input file
    Protected Sub DoOneFile(ByVal filename As String)

        Try
            ConvertMessages(filename)

            LogFile.WriteLog(logFolder, "Output XML written from " & Path.GetFileName(filename))
            File.Copy(filename, FuncLib.MakeSubDirDate(doneFolder & "\" & Path.GetFileName(filename), File.GetLastWriteTime(filename)), True)

        Catch ex As Exception
            'Log error
            LogFile.WriteErr(logFolder, "Error writing output XML from " & Path.GetFileName(filename), ex)
            File.Copy(filename, FuncLib.MakeSubDirDate(errorFolder & "\" & Path.GetFileName(filename), File.GetLastWriteTime(filename)), True)
        End Try

        'Cleanup
        Try
            File.Delete(filename)
        Catch ex As Exception
            'Log error
            LogFile.WriteErr(logFolder, "Cleanup failed", ex)
        End Try

    End Sub

    Protected Function ConvertMessages(ByVal filename As String)

        Dim messages As String()

        'Read file and split
        Try
            Dim inn As StreamReader = New StreamReader(filename, Encoding.GetEncoding("iso-8859-1"))
            Dim data As String = inn.ReadToEnd()
            inn.Close()

            'Char bullshit
            data = data.Replace(ChrW(145), "�"c)
            data = data.Replace(ChrW(146), "�"c)

            data = data.Replace(ChrW(155), "�"c)
            data = data.Replace(ChrW(157), "�"c)

            data = data.Replace(ChrW(134), "�"c)
            data = data.Replace(ChrW(143), "�"c)

            data = data.Replace(ChrW(25), "")

            'XML stuff
            data = data.Replace("&", "&amp;")

            messages = Regex.Split(data, "\$et")
        Catch ex As Exception

        End Try

        'Matching expressions
        Dim rxTit As Regex = New Regex("^<m:if>(.+)" & vbCrLf, RegexOptions.Multiline)
        Dim rxSo As Regex = New Regex("^\$st(.+)" & vbCrLf, RegexOptions.Multiline)
        Dim rxID As Regex = New Regex("^\$na(.+)" & vbCrLf, RegexOptions.Multiline)

        'Loop parts
        Dim err As Boolean = False
        Dim msg As String
        For Each msg In messages

            err = False
            If msg <> "" And msg.Length() > 20 Then

                Dim fn As String
                Dim output As StringBuilder = New StringBuilder

                'Conversion
                Try
                    'Parts
                    Dim hd As String = Regex.Split(msg, "\$sx" & vbCrLf)(0)
                    Dim bd As String = Regex.Split(msg, "\$sx" & vbCrLf)(1)

                    'Header data
                    Dim title As String = rxTit.Match(bd).Groups(1).Value()
                    Dim so As String = rxSo.Match(hd).Groups(1).Value()
                    Dim idext As String = rxID.Match(hd).Groups(1).Value()

                    so = Regex.Replace(so, "[ \.,_]", "-").ToLower()
                    so = Regex.Replace(so, "-+", "-")

                    fn = idext & "-" & so & ".xml"

                    'Special char fix
                    fn = Regex.Replace(fn, "[������]", "-")

                    'Create Body
                    Dim body As String
                    If filename.IndexOf("titopp") > -1 Then
                        body = CreateNITF32TableBody(bd)
                    Else
                        body = CreateNITF32TextBody(bd)
                    End If

                    'Create Header
                    Dim header As String = CreateNITF32Header(title, so, idext, File.GetLastWriteTime(filename), bd.Length, fn)

                    'Assemble data
                    output.Append("<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & vbCrLf)
                    output.Append("<!--<!DOCTYPE nitf SYSTEM ""nitf-3-2.dtd"">-->" & vbCrLf)
                    output.Append("<nitf version=""-//IPTC//DTD NITF 3.2//EN"" change.date=""October 10, 2003"" change.time=""19:30"" baselang=""no-NO"">" & vbCrLf)

                    output.Append(header)
                    output.Append(body)

                    output.Append("</nitf>")

                Catch ex As Exception
                    'Conversion failed, log
                    LogFile.WriteErr(logFolder, "Conversion failed, cannot write XML: " & fn, ex)
                    err = True
                End Try

                'Save data
                If Not err Then

                    Try
                        Dim sw As StreamWriter = New StreamWriter(outputFolder & "/" & fn, False, Encoding.GetEncoding("iso-8859-1"))
                        sw.WriteLine(output.ToString)
                        sw.Close()

                        LogFile.WriteLog(logFolder, "XML written : " & fn)
                    Catch ex As Exception
                        'Saving failed
                        LogFile.WriteErr(logFolder, "Saving failed, cannot write XML: " & fn, ex)
                    End Try

                End If
            End If

        Next

    End Function

    Protected Function CreateNITF32Header(ByVal title As String, ByVal so As String, ByVal idext As String, ByVal timestamp As DateTime, ByVal length As Integer, ByVal fn As String) As String

        Dim output As StringBuilder = New StringBuilder
        Dim id As String = "DFS:" & timestamp.ToString("yyyyMMdd:mmss:") & idext

        output.Append("<head>" & vbCrLf)
        output.Append("<title>" & title & "</title>" & vbCrLf)

        output.Append("<meta name=""timestamp"" content=""" & timestamp.ToString("yyyy.MM.dd HH:mm:ss") & """ />" & vbCrLf)

        output.Append("<meta name=""subject"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""foldername"" content=""Ut-Satellitt"" />" & vbCrLf)
        output.Append("<meta name=""filename"" content=""" & fn & """ />" & vbCrLf)

        output.Append("<meta name=""NTBTjeneste"" content=""Nyhetstjenesten"" />" & vbCrLf)
        output.Append("<meta name=""NTBMeldingsSign"" content=""dfs/"" />" & vbCrLf)
        output.Append("<meta name=""NTBPrioritet"" content=""5"" />" & vbCrLf)
        output.Append("<meta name=""NTBStikkord"" content=""" & so & """ />" & vbCrLf)
        output.Append("<meta name=""NTBDistribusjonsKode"" content=""ALL"" />" & vbCrLf)
        output.Append("<meta name=""NTBKanal"" content=""C"" />" & vbCrLf)
        output.Append("<meta name=""NTBIPTCSequence"" content=""" & Get_Seq() & """ />" & vbCrLf)
        output.Append("<meta name=""NTBID"" content=""" & id & """ />" & vbCrLf)

        output.Append("<meta name=""ntb-dato"" content=""" & timestamp.ToString("dd.MM.yyyy HH:mm") & """ />" & vbCrLf)

        output.Append("<tobject tobject.type=""Sport"">" & vbCrLf)
        output.Append("<tobject.property tobject.property.type=""Tabeller og resultater"" />" & vbCrLf)
        output.Append("<tobject.subject tobject.subject.code=""SPO"" tobject.subject.refnum=""15000000"" tobject.subject.type=""Sport"" />" & vbCrLf)
        output.Append("<tobject.subject tobject.subject.code=""SPO"" tobject.subject.refnum=""15051000"" tobject.subject.matter=""Skyting"" />" & vbCrLf)
        output.Append("</tobject>" & vbCrLf)

        output.Append("<docdata>" & vbCrLf)
        output.Append("<doc-id regsrc=""NTB"" id-string=""" & id & """/>" & vbCrLf)
        output.Append("<urgency ed-urg=""6""/>" & vbCrLf)
        output.Append("<date.issue norm=""" & timestamp.ToString("yyyy-MM-ddTHH:mm:ss") & """/>" & vbCrLf)
        output.Append("<ed-msg info="""" />" & vbCrLf)
        output.Append("<du-key version=""1"" key=""" & so & """ />" & vbCrLf)
        output.Append("<doc.copyright year=""" & timestamp.ToString("yyyy") & """ holder=""NTB"" /> " & vbCrLf)
        output.Append("<key-list>" & vbCrLf)
        output.Append("<keyword key=""" & so & """ />" & vbCrLf)
        output.Append("</key-list>" & vbCrLf)
        output.Append("</docdata>" & vbCrLf)

        output.Append("<pubdata date.publication=""" & timestamp.ToString("yyyyMMddTHHmmss") & """ item-length=""" & length & """ unit-of-measure=""character"" />" & vbCrLf)

        output.Append("<revision-history name=""dfs/"" />" & vbCrLf)

        output.Append("</head>" & vbCrLf)

        Return output.ToString()

    End Function

    Protected Function CreateNITF32TextBody(ByVal text As String) As String

        Dim output As StringBuilder = New StringBuilder

        Dim rxTit As Regex = New Regex("^<m:if>(.+)" & vbCrLf, RegexOptions.Multiline)
        Dim title As String = rxTit.Match(text).Groups(1).Value()

        output.Append("<body>" & vbCrLf)

        output.Append("<body.head>" & vbCrLf)
        output.Append("<hedline>" & vbCrLf)
        output.Append("<hl1>" & title & "</hl1>" & vbCrLf)
        output.Append("</hedline>" & vbCrLf)
        output.Append("<distributor><org>NTB</org></distributor>" & vbCrLf)
        output.Append("</body.head>" & vbCrLf)

        output.Append("<body.content>" & vbCrLf)

        Dim lines As String() = Regex.Split(text, vbCrLf)

        Dim sTag, eTag As String
        Dim l As String

        For Each l In lines

            l = l.Replace("<eop>", "")

            If l.StartsWith("<m:ov>") Then
                'Overskrift
                l = l.Substring(l.LastIndexOf(">") + 1)
                sTag = "<hl2>"
                eTag = "</hl2>"

            ElseIf l.StartsWith("<m:te><em>") Then
                'Tekst, inrykk
                l = l.Substring(l.LastIndexOf(">") + 1)
                sTag = "<p class=""txt-ind"">"
                eTag = "</p>"

            ElseIf l.StartsWith("<m:te>") Then
                'Tekst 
                l = l.Substring(l.LastIndexOf(">") + 1)
                sTag = "<p class=""txt"">"
                eTag = "</p>"

            ElseIf l.StartsWith("<em>") Then
                'Tag removal
                l = l.Substring(l.LastIndexOf(">") + 1)

            ElseIf l.StartsWith("<m:if>") Then
                'Skip infolinje in body
                l = ""
            End If

            If l <> "" Then output.Append(sTag & l & eTag & vbCrLf)

        Next

        output.Append("</body.content>" & vbCrLf)

        output.Append("<body.end>" & vbCrLf)
        output.Append("<tagline><a href=""mailto:sporten@ntb.no"">sporten@ntb.no</a></tagline>" & vbCrLf)
        output.Append("</body.end>" & vbCrLf)

        output.Append("</body>" & vbCrLf)

        Return output.ToString()
    End Function

    Protected Function CreateNITF32TableBody(ByVal text As String) As String

        Dim output As StringBuilder = New StringBuilder

        Dim rxTit As Regex = New Regex("^<m:if>(.+)" & vbCrLf, RegexOptions.Compiled)
        Dim rxTblCode As Regex = New Regex("^\[([\w\d]+)\]", RegexOptions.Compiled)
        'Dim rxTblLine As Regex = New Regex("^\{" & ChrW(25) & "?([\d\w]+\.)\s�([\w������\-\. ]+)\s+\}�([\w������\-\. ]+)\s+�([\w\d\.������]+)�", RegexOptions.Compiled)

        Dim rxTblLine As Regex = New Regex("^\{([\d\w]+\.)\s�([\w������\-\. ]+)\s*\}�([\w������/\-\.;&\(\) ]+)\s*�([\w\d\.\+/������ ]+)[��]", RegexOptions.Compiled)

        Dim title As String = rxTit.Match(text).Groups(1).Value()

        output.Append("<body>" & vbCrLf)

        output.Append("<body.head>" & vbCrLf)
        output.Append("<hedline>" & vbCrLf)
        output.Append("<hl1>" & title & "</hl1>" & vbCrLf)
        output.Append("</hedline>" & vbCrLf)
        output.Append("<distributor><org>NTB</org></distributor>" & vbCrLf)
        output.Append("</body.head>" & vbCrLf)

        output.Append("<body.content>" & vbCrLf)

        Dim lines As String() = Regex.Split(text, vbCrLf)

        Dim code As String
        Dim tblOpen As Boolean = False
        Dim l As String

        For Each l In lines

            l = l.Replace("<eop>", "")

            If rxTblCode.IsMatch(l) Then

                code = rxTblCode.Match(l).Groups(1).Value()

                If code.StartsWith("x") And tblOpen Then
                    output.Append("</table>" & vbCrLf)
                    tblOpen = False
                Else
                    output.Append("<p class=""table-code"">[" & code & "]</p>" & vbCrLf)
                End If

            ElseIf rxTblLine.IsMatch(l) Then

                If Not tblOpen Then
                    output.Append("<table class=""4-col"">" & vbCrLf)
                    tblOpen = True
                End If

                output.Append("<tr>" & vbCrLf)
                output.Append("<td>" & rxTblLine.Match(l).Groups(1).Value() & "</td>")
                output.Append("<td>" & rxTblLine.Match(l).Groups(2).Value() & "</td>")
                output.Append("<td>" & rxTblLine.Match(l).Groups(3).Value() & "</td>")
                output.Append("<td>" & rxTblLine.Match(l).Groups(4).Value() & "</td>")
                output.Append("</tr>" & vbCrLf)

            ElseIf l = "�" Then
                output.Append("</table>" & vbCrLf)
                tblOpen = False

            ElseIf l.StartsWith("<m:if>") Or l.StartsWith("<m:te>") Then
                'Skip infolinje and text code in body

            ElseIf l <> "" Then
                output.Append("<p class=""notext"">" & l & "</p>" & vbCrLf)
            End If


        Next

        output.Append("</body.content>" & vbCrLf)

        output.Append("<body.end>" & vbCrLf)
        output.Append("<tagline><a href=""mailto:sporten@ntb.no"">sporten@ntb.no</a></tagline>" & vbCrLf)
        output.Append("</body.end>" & vbCrLf)

        output.Append("</body>" & vbCrLf)

        Return output.ToString()
    End Function

End Class
